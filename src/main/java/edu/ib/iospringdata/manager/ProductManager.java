package edu.ib.iospringdata.manager;


import edu.ib.iospringdata.dao.ProductRepo;
import edu.ib.iospringdata.dao.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class ProductManager {

    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }

    public Iterable<Product> findAll(){
        return productRepo.findAll();
    }

    public Product save(Product product){
        return productRepo.save(product);
    }

    public void saveFromPatch(Product product, Long id){
       Product product1 = productRepo.findById(id).get();
       boolean needUpdate = false;
       if(StringUtils.hasLength(product.getName())){
           product1.setName(product.getName());
           needUpdate = true;
       }
       if(product.getPrice()!=product1.getPrice()){
           product1.setPrice(product.getPrice());
           needUpdate = true;
       }
       if(product.isAvailable() != product1.isAvailable()){
           product1.setAvailable(product.isAvailable());
           needUpdate = true;
       }
       if(needUpdate){
           productRepo.save(product1);
       }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        save(new Product("serek miejski",4.40f,true));
        save(new Product("jaja",9.5f,true));
        save(new Product("cukier",50.5f,false));
    }
}
