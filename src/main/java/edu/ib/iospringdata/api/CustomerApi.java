package edu.ib.iospringdata.api;

import edu.ib.iospringdata.dao.entity.Customer;
import edu.ib.iospringdata.manager.CustomerManager;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class CustomerApi {

    private CustomerManager customers;


    public CustomerApi(CustomerManager customers) {
        this.customers = customers;
    }
    @GetMapping("/customer/all")
    public Iterable<Customer> getAllCustomers(){
        return customers.findAll();
    }


    @GetMapping("/customer")
    public Optional<Customer> getByIdCustomer(@RequestParam Long id){
        return  customers.findById(id);

    }

    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        return customers.save(customer);

    }
    @PutMapping("/admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer){
        return customers.save(customer);

    }
    @PatchMapping("/admin/customer")
    public void patchCustomer(@RequestParam Long id, @RequestBody Customer customer){
        customers.saveFromPatch(customer, id);

    }

}
