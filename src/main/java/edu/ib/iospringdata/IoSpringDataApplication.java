package edu.ib.iospringdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
public class IoSpringDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(IoSpringDataApplication.class, args);
    }

}
