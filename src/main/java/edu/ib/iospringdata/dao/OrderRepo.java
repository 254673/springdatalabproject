package edu.ib.iospringdata.dao;

import edu.ib.iospringdata.dao.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRepo extends JpaRepository<Order, Long> {
}
