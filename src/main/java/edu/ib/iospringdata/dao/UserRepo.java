package edu.ib.iospringdata.dao;

import edu.ib.iospringdata.dao.entity.UserDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserDto,Long> {

    UserDto getUserDtoByName(String name);
}
