.. IOSprongData documentation master file, created by
   sphinx-quickstart on Mon May 30 12:04:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witaj w dokumentacji projektu IOSpringData
============================================

.. toctree::
   :maxdepth: 2
   :caption: Zawartość:

   Główna <self>
   Wprowadzenie <wprowadzenie>
   Instalacja <instalacja>
   Architektura <architektura>
   REST api <api>





