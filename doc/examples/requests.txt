GET http://localhost:8080/api/product/all

GET http://localhost:8080/api/product?id=1

POST http://localhost:8080/api/admin/product
{
    "name": "kakao Puchatek",
     "price": 9.99,
     "available": true
}

PUT http://localhost:8080/api/admin/product
{
        "id": 3,
        "name": "serek wiejski",
        "price": 1.99,
        "available": false
    }

PATCH http://localhost:8080/api/admin/product?id=1
{
        "price": 15.40,
        "available": false
    }

GET http://localhost:8080/api/customer/all

GET http://localhost:8080/api/customer?id=1

POST http://localhost:8080/api/admin/customer
{
     "name": "Marek Grechuta",
     "address": "Warszawa"
}

PUT http://localhost:8080/api/admin/customer
{
        "id": 1,
        "name": "Kamil Slimak",
        "address": "Bydgoszcz"
    }

PATCH http://localhost:8080/api/admin/customer?id=1
{
        "address": "Legnica"
    }

GET http://localhost:8080/api/order?id=1

GET http://localhost:8080/api/order/all

POST http://localhost:8080/api/order
 {
        "customer": {
            "name": "Krzysztof Gabanewicz",
            "address": "Szczecin"
        },
        "products": [
            {
                "name": "sok Kubus",
                "price": 4.3,
                "available": true
            } 
        ],
        "placeDate": "2022-04-24T21:17:58.885706",
        "status": "accepted"
    }

PUT http://localhost:8080/api/admin/order
{
        "id": 1,
        "customer": {
            "name": "Anna Nowakowska",
            "address": "Morzeszczyn"
        },
        "products": [
            {
                "id": 1,
                "name": "maslo",
                "price": 15.99,
                "available": true
            },
            {
                "id": 2,
                "name": "Kustosz Mocny",
                "price": 1.5,
                "available": true
            }
        ],
        "placeDate": "2022-04-24T21:17:58.885706",
        "status": "resigned"
    }

PATCH http://localhost:8080/api/admin/order?id=1
{

        "status": "finished"
    }